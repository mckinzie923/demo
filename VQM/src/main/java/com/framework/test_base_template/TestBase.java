package com.framework.test_base_template;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeMethod;
import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.Hashtable;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.framework.actions.WebAction;
import com.functions.CommonFunctionLib;
import com.functions.Reader;
import com.riot.pages.LandingPage;
import java.util.concurrent.TimeUnit;

/**
 * TestBase.java is inherited by all test classes,
 */

public class TestBase extends Exception {
	static {
		System.setProperty("org.uncommons.reportng.escape-output", "false");
	}

	private static final long serialVersionUID = 1L;
	protected LandingPage landingpage;
	protected WebDriver driver;
	protected WebAction action;
	public static Properties TestExecution = new Properties();
	Reader xls;
	protected int sheet = 0;

	/**
	 * Purpose: constructor for setting properties file
	 * 
	 * @throws Exception
	 */
	protected TestBase() throws Exception {
		FileInputStream fs;
		try {
			fs = new FileInputStream(CommonFunctionLib.userDir + "\\src\\test\\resources\\TestExecution.properties");
			TestExecution.load(fs);
		} catch (FileNotFoundException e) {
			throw new Exception("Error in connecting with Remote Webdriver" + e);
		} catch (IOException e) {
			throw new Exception("Error in connecting with Remote Webdriver" + e);
		}

	}

	
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Binary\\chromedriver.exe");
		driver = new ChromeDriver();
		WebAction action = new WebAction(driver);
		landingpage = new LandingPage(driver, action);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.navigate().to("https://saqat.t-mobile.com/sites/riotpilot/");
	}
	@AfterMethod
	public void destor(ITestResult result) throws IOException {

	}

	@DataProvider
	protected Object[][] readData(Method method) {
		xls = new Reader(sheet);
		return xls.readData(method.getName(), xls);
	}

	@DataProvider
	protected Object[][] readDataNew(String method) {
		xls = new Reader(2);
		return xls.readData(method, xls);
	}

	public Hashtable<String, String> readDataFromSheet(String testName, int sheet) {
		xls = new Reader(sheet);
		return xls.readData1(testName, xls);
	}

	//
	@AfterMethod
	public void afterMethod(ITestResult result, Method method) throws Exception, IOException, AWTException {
	}

	@AfterSuite
	public void afterSuit() {

	}
}