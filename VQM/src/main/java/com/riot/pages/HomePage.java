package com.riot.pages;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.framework.actions.WebAction;
//import com.framework.exception.SeleniumException;
import com.functions.CommonFunctionLib;
//import com.pier.pageObjects.HomePageObjects;
import com.riot.pageObjects.HomePageObjects;

public class HomePage extends LandingPage {

	private HomePageObjects obj;
	public HomePage(WebDriver driver, WebAction action) {
		super(driver,action);
		
		obj = new HomePageObjects(driver);
		

		//homeObj = new HomePageObjects(driver);
	}
	
	/*public HomePage verifyHomePage() throws IOException, Exception {
		CommonFunctionLib.waitUntilTrue(() -> action.isDisplayed(obj.linkHome), 5);

		CommonFunctionLib.waitUntilTrue(() -> {
			return driver.getTitle().trim().equals(obj.pageTitle.trim());
		}, 5);

		CommonFunctionLib.assertTrue(driver.getTitle().trim().equals(obj.pageTitle.trim()), "Home Page not found!",
				driver);
		CommonFunctionLib.log("Verify user is on 'Home Page'", driver);

		return new HomePage(driver, action);
	}*/
	

	/*public MasterTicketPage gotoTicketPage() throws SeleniumException, IOException {
		action.waitAndClick(homeObj.lblTicket);
		CommonFunctionLib.log("Click on 'Ticket' menu under quick links", driver);
		return new MasterTicketPage(driver, action);
	}

	public MasterChangeRecordPage gotoChangeRecordPage() throws SeleniumException, IOException {
		action.waitAndClick(homeObj.lblChangeRecord);
		CommonFunctionLib.log("Click on 'Change Record' menu under quick links", driver);
		return new MasterChangeRecordPage(driver, action);
	}
	*/

	protected boolean waitUntilTrue(Boolean condition, int secounds) {
		boolean val = false;
		while (true) {
			try {
				if (condition) {
					val = true;
					break;
				} else {
					secounds--;
					CommonFunctionLib.sleep(1);
				}

				if (secounds == 0) {
					break;
				}

			} catch (Exception e) {
				secounds--;
				CommonFunctionLib.sleep(1);
				if (secounds == 0) {
					break;
				}
			}
		}
		return val;
	}

/*	public boolean waitForJStoLoad() {

	    WebDriverWait wait = new WebDriverWait(driver, 30);

	    // wait for jQuery to load
	    ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
	      @Override
	      public Boolean apply(WebDriver driver) {
	        try {
	          return ((Long)((JavascriptExecutor)driver)("return jQuery.active") == 0);
	        }
	        catch (Exception e) {
	          return true;
	        }
	      }
	    };

	    // wait for Javascript to load
	    ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
	      @Override
	      public Boolean apply(WebDriver driver) {
	        return executeJavaScript("return document.readyState")
	            .toString().equals("complete");
	      }
	    };

	  return wait.until(jQueryLoad) && wait.until(jsLoad);
	}
	*/
	protected void implecitWait(int secounds){
		driver.manage().timeouts().implicitlyWait(secounds, TimeUnit.SECONDS);
	}
	
	
	
	/*public HomePage Login(String userName, String password) {
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (Exception e) {
			e.printStackTrace();
		}

		CommonFunctionLib.copyToClipboard(userName);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);

		CommonFunctionLib.sleep(1);

		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);

		CommonFunctionLib.copyToClipboard(password);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);

		CommonFunctionLib.sleep(1);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		
		return new HomePage(driver, action);
		
	}*/

	public HomePage verifyHomepage() throws IOException {
		int count = 0;
		while (true) {
			boolean val = false;
			try {
				val = waitUntilTrue(driver.getCurrentUrl().contains("Home"), 1);
				if (driver.getCurrentUrl().contains("Dashboard")) {
				//	action.click(homeObj.lblHome);
				}
				if (val) {
					break;
				} else {
					count++;
					CommonFunctionLib.sleep(1);
					if (count == 20) {
						count = 0;
						driver.navigate().refresh();
						// break;
					}
				}
			} catch (Exception e) {
				count++;
				CommonFunctionLib.sleep(1);
				if (count == 20) {
					count = 0;
					driver.navigate().refresh();
					// break;
				}
			}

		}

		count = 0;
		while (true) {
			boolean val1 = false;
			try {
				//val1 = action.isClickable(homeObj.lblLinkToOterTools);
				if (val1) {
					break;
				} else {
					count++;
					CommonFunctionLib.sleep(1);
					if (count == 20) {
						count = 0;
						break;
					}
				}
			} catch (Exception e) {
				count++;
				CommonFunctionLib.sleep(1);
				if (count == 20) {
					count = 0;
					break;
				}
			}

		}

		//CommonFunctionLib.assertTrue(driver.getCurrentUrl().contains("Home"), "Cant not find Home Page", driver);
		//CommonFunctionLib.log("Verify user is on 'Home Page'", driver);

		// waitForElements();
		return new HomePage(driver, action);
	}

	/*public HomePage waitForElements() throws SeleniumException, IOException {
		int count = 0;
		while (true) {
			boolean val = false;
			try {
				System.err.println(count);
				val = waitUntilTrue(action.isDisplayed(homeObj.lblHome), 1);
				if (val) {
					break;
				} else {
					count++;
					CommonFunctionLib.sleep(1);
					if (count == 4) {
						count = 0;
						driver.navigate().refresh();
						break;
					}
				}
			} catch (Exception e) {
				count++;
				CommonFunctionLib.sleep(1);
				if (count == 2) {
					count = 0;
					driver.navigate().refresh();
					break;
				}
			}

		}

		CommonFunctionLib.assertTrue(driver.getCurrentUrl().contains("Home"), "Cant not find Home Page", driver);
		CommonFunctionLib.log("Verify user is on 'Home Page'", driver);

		return new HomePage(driver, action);
	}

	protected void selectItemFromDropDown(int itemIndex, WebElement list) throws SeleniumException, IOException {
		try {
			int count = 0;
			while (true) {
				try {
					action.click(homeObj.getDropItems(list).get(itemIndex));
					count = 0;
					break;
				} catch (Exception e) {
					count++;
					CommonFunctionLib.sleep(1);
					if (count == 20) {
						count = 0;
						break;
					}
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	protected void waitUntilMsgDissAppier(){
		while (true) {
			int count = 0;
			try {
				if (action.getText(homeObj.msgDiv).contains("")) {
					count = 0;
					break;
				} else {
					count++;
					CommonFunctionLib.sleep(1);
					if (count == 7) {
						count = 0;
						break;
					}
				}
			} catch (Exception e) {
				count = 0;
				break;
			}
		}

		CommonFunctionLib.sleep(4);
	}
	
	protected void selectItemFromDropDown(WebElement dropDown, int index, WebElement list, String text)
			throws SeleniumException, IOException {
		WebElement el = null;
		String dropItemTxt;
		action.waitAndClick(dropDown);
		try {
			int count = 0;
			while (true) {
				try {
					el = homeObj.getDropItems(list).get(index);
					count = 0;
					break;
				} catch (Exception e) {
					count++;
					CommonFunctionLib.sleep(1);
					if (count == 20) {
						count = 0;
						break;
					}
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		dropItemTxt = action.getText(el);

		action.waitAndClick(el);

		CommonFunctionLib.log("Select '" + dropItemTxt + "' from " + text + " dropdown", driver);
	}

	protected void waitForProcess(int secounds) {
		int count = 0;
		while (true) {
			try {
				WebElement el = homeObj.process;
				if (action.isDisplayed(el)) {
					CommonFunctionLib.sleep(secounds);
					count++;
					if (count == secounds) {
						count = 0;
						break;
					}
				} else {
					count = 0;
					break;
				}
			} catch (Exception e) {
				break;
			}
		}
	}
	
	
	protected void verifyTicketMsg(String msg) throws SeleniumException, IOException {
		if (action.isDisplayed(homeObj.msgDiv)) {
			CommonFunctionLib.assertTrue(action.getText(homeObj.msgDiv).contains(msg), msg + ", message not found!",
					driver);
			CommonFunctionLib.log("Verify message: '" + action.getText(homeObj.msgDiv) + "' is displayed", driver);
		} else {
			throw new SeleniumException(msg + ", message not found!");
		}
		
	}

	protected boolean verifyTicketMsg(int i, String msg) throws SeleniumException, IOException {
		if (action.isDisplayed(homeObj.msgDiv)) {
			CommonFunctionLib.assertTrue(action.getText(homeObj.msgDiv).contains(msg), msg + ", message not found!",
					driver);
			return true;
		} else {
			return false;
		}
	}*/

	/*
	 * public CreateTicketPage clickOnCreateTicketIcon() throws
	 * SeleniumException, IOException{ action.click(homeObj.imgCreateTicket);
	 * CommonFunctionLib.log("Click on 'Create Ticket' image", driver); return
	 * new CreateTicketPage(driver, action); }
	 */

	/*
	 * public void jiraIntigration() { JerseyJiraRestClientFactory f = new
	 * JerseyJiraRestClientFactory(); JiraRestClient jc =
	 * f.createWithBasicHttpAuthentication(new URI("http://192.168.4.139:8080"),
	 * "vipul.kotadiya", "softweb#123");
	 * 
	 * SearchResult r = jc.getSearchClient().searchJql(
	 * "type = Epic ORDER BY RANK ASC", null);
	 * 
	 * Iterator<BasicIssue> it = r.getIssues().iterator(); while (it.hasNext())
	 * {
	 * 
	 * Issue issue =
	 * jc.getIssueClient().getIssue(((BasicIssue)it.next()).getKey(), null);
	 * 
	 * System.out.println("Epic: " + issue.getKey() + " " + issue.getSummary());
	 * 
	 * Iterator<IssueLink> itLink = issue.getIssueLinks().iterator(); while
	 * (itLink.hasNext()) {
	 * 
	 * IssueLink issueLink = (IssueLink)itLink.next(); Issue issueL =
	 * jc.getIssueClient().getIssue((issueLink).getTargetIssueKey(), null);
	 * 
	 * System.out.println(issueLink.getIssueLinkType().getDescription() + ": " +
	 * issueL.getKey() + " " + issueL.getSummary() + " " +
	 * issueL.getFieldByName("Story Points").getValue());
	 * 
	 * }
	 * 
	 * } }
	 */

}
