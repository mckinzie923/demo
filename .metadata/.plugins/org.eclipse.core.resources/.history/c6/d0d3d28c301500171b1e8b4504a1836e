package com.framework.test_base_template;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.exostar.pages.CSQQuestionnairePage;
import com.exostar.pages.LandingPage;
import com.exostar.pages.NISTQuestionnairePage;
import com.framework.actions.WebAction;
import com.functions.CaptureBrowserScreenShot;
import com.functions.CommonFunctionLib;
import com.functions.Reader;

/**
 * TestBase.java is inherited by all test classes,
 */

public class TestBase extends Exception {
	static {
		System.setProperty("org.uncommons.reportng.escape-output", "false");
	}

	private static final long serialVersionUID = 1L;
	public static Properties TestExecution = new Properties();
	Reader xls;
	CaptureBrowserScreenShot captureBrowserScreenShot = new CaptureBrowserScreenShot();
	protected int sheet = 0;
	/*
	 * protected Hashtable<String, String> supplierOrg2UserId =
	 * getSupplierOrg2UserId(); protected Hashtable<String, String>
	 * NistFileUploadUser = getFileUploadUser();
	 */
	protected Hashtable<String, String> orgDetails = getOrgDetails();
	protected Hashtable<String, String> buyerAndEpaCredentials = readDataFromSheet("BuyerAndEpaCredentials", 2);
	protected Hashtable<String, String> newOrgDetails = readDataFromSheet("NewOrganizationDetails", 2);
	protected String MAG_URL = "";
	protected String eGRC_URL = "";

	/**
	 * Purpose: constructor for setting properties file
	 * 
	 * @throws Exception
	 */
	protected TestBase() throws Exception {
		System.out.println("here!");
		FileInputStream fs;
		try {
			fs = new FileInputStream(CommonFunctionLib.userDir + "\\src\\test\\resources\\TestExecution.properties");
			TestExecution.load(fs);
		} catch (FileNotFoundException e) {
			throw new Exception("Error in connecting with Remote Webdriver" + e);
		} catch (IOException e) {
			throw new Exception("Error in connecting with Remote Webdriver" + e);
		}

	}

	@BeforeSuite
	public void beforeSuit() throws MalformedURLException {
	}

	@BeforeMethod
	public void setUp(Method m) throws IOException, Exception {

		String browser = TestExecution.getProperty("BROWSER");

		if (browser.equals("FireFox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\Binary\\geckodriver.exe");
			ProfilesIni getAll = new ProfilesIni();
			driver = new FirefoxDriver(getAll.getProfile("default"));
		} else if (browser.equals("Chrome")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\Binary\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("start-maximized");
			driver = new ChromeDriver(options);

		} else {
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\Binary\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}

		WebAction action = new WebAction(driver);
		landingPage = new LandingPage(driver, action);
		// driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		CommonFunctionLib.setimplicitlyWait(15, driver);

		if (m.getName().equals("selfRegistrationTest")) {

			driver.navigate().to(TestExecution.getProperty("SelfReistration"));
			CommonFunctionLib.log(
					"Navigate to 'https://tprqa.exostartest.com/credmgr/pages/registration/orgRegistration.faces' URL",
					driver);
		} else {

			String platform = TestExecution.getProperty("PLATFORM");
			String URL = "";
			if (platform.equals("QA")) {
				URL = TestExecution.getProperty("QAeGRC");
				MAG_URL = TestExecution.getProperty("QA_MAG");
				eGRC_URL = TestExecution.getProperty("QA_eGRC");
			} else if (platform.equals("UAT")) {
				URL = TestExecution.getProperty("UATeGRC");
				MAG_URL = TestExecution.getProperty("UAT_MAG");
				eGRC_URL = TestExecution.getProperty("UAT_eGRC");
			}

			try {
				driver.navigate().to(URL);
			} catch (WebDriverException e) {
				CommonFunctionLib.confirmSecurityException(action, driver);
				CommonFunctionLib.confirmSecurityException(action, driver);
			}

			CommonFunctionLib.log("Navigete to '" + URL + "' URL", driver);
		}

	}

	protected Hashtable<String, String> getSupplierOrg2AdminId() {
		return readDataFromSheet("SupplierOrg2Admin", 0);
	}

	protected Hashtable<String, String> getSupplierOrg2UserId() {
		return readDataFromSheet("SupplierOrg2User", 0);
	}

	protected Hashtable<String, String> getFileUploadUser() {
		return readDataFromSheet("NistFileUpload", 0);
	}

	protected Hashtable<String, String> getGroupName() {
		Hashtable<String, String> groupName = readDataFromSheet("GroupName", 1);

		String group = groupName.get("Name");
		int number = Integer.parseInt(group.replaceAll("\\D+", ""));
		number++;
		group = group.replaceAll("\\d+.*", Integer.toString(number));

		try {
			Reader.write(group, 1, 2, 0);
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			e.printStackTrace();
		}

		return groupName;
	}

	private Hashtable<String, String> getOrgDetails() {
		Hashtable<String, String> orgDetails = readDataFromSheet("OrganizationInformation", 2);
		CommonFunctionLib.orgName = orgDetails.get("Name");
		CommonFunctionLib.orgNumber = Integer.parseInt(CommonFunctionLib.orgName.replaceAll("\\D+", ""));
		return orgDetails;
	}

	@AfterMethod
	public void destor(ITestResult result) throws IOException {

		if (driver != null) {

			/*
			 * Runtime.getRuntime().exec("taskkill /F /IM firefox.exe");
			 * Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe"
			 * ); Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe");
			 */

			if (result.getStatus() == ITestResult.SUCCESS) {
				driver.quit();
			}

		}

	}

	@DataProvider
	protected Object[][] readData(Method method) {
		xls = new Reader(sheet);
		return xls.readData(method.getName(), xls);
	}

	@DataProvider
	protected Object[][] readDataNew(String method) {
		xls = new Reader(2);
		return xls.readData(method, xls);
	}

	public Hashtable<String, String> readDataFromSheet(String testName, int sheet) {
		xls = new Reader(sheet);
		return xls.readData1(testName, xls);
	}

	
	protected void updateOrgInfo() {
		int size;
		size = readDataNew("NewOrganizationDetails").length;
		String orgName = CommonFunctionLib.orgName.replaceAll("\\d+.*",
				Integer.toString(CommonFunctionLib.orgNumber + 1));
		try {
			Reader.write(CommonFunctionLib.newOrgDetails.get("Buyer"), 2, ((size) + 28), 0);
			Reader.write(CommonFunctionLib.newOrgDetails.get("SupplierOrgName"), 2, (size) + 28, 1);
			Reader.write(CommonFunctionLib.newOrgDetails.get("OrgAdmin"), 2, (size) + 28, 2);
			Reader.write(CommonFunctionLib.newOrgDetails.get("Email"), 2, (size) + 28, 3);
			Reader.write(CommonFunctionLib.newOrgDetails.get("UserId"), 2, (size) + 28, 5);
			Reader.write(CommonFunctionLib.newOrgDetails.get("Password"), 2, (size) + 28, 6);
			Reader.write(orgName, 2, 2, 0);
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			e.printStackTrace();
		}
	}

	//
	@AfterMethod
	public void afterMethod(ITestResult result, Method method) throws Exception, IOException, AWTException {
	}

	@AfterSuite
	public void afterSuit() {

	}

	protected CSQQuestionnairePage goToCSQForm(String userName, String password) throws IOException, Exception {

		return landingPage.login(userName, password).verifyHomePage().gotoCSQQuestionnairePage().verifyPage();
	}

	protected NISTQuestionnairePage gotoNISTForm(String userName, String password) throws IOException, Exception {

		return landingPage.login(userName, password).verifyHomePage().gotoNISTQuestionnairePage().verifyPage();
	}

}